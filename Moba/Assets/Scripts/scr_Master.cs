﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using Photon.Pun;
using Photon.Realtime;

public class scr_Master : MonoBehaviourPunCallbacks
{
    public TextMeshProUGUI stats;
    public Button b_ConnectToMaster;
    public Button b_ConnectToGame;

    private void Awake()
    {
        b_ConnectToGame.gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        stats.text = "Ping: " + PhotonNetwork.GetPing() + "\n" + "Connected: " + PhotonNetwork.IsConnected;
    }

    public void OnClickConnectToServer()
    {
        PhotonNetwork.NickName = "Player001";
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.GameVersion = "v0";
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        Debug.Log("Conected To Serer Master");

        b_ConnectToMaster.gameObject.SetActive(false);
        b_ConnectToGame.gameObject.SetActive(true);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        Debug.Log("Disconnected: " + cause);
    }

    public void OnClickConnectToRoom()
    {
        if (!PhotonNetwork.IsConnected)
            return;

        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("Joined Room");
        Debug.Log("MASTER: " + PhotonNetwork.IsMasterClient + " | Player Name: " + PhotonNetwork.NickName + " | Room Name: " + PhotonNetwork.CurrentRoom.Name + " | Room Players: " + PhotonNetwork.CurrentRoom.PlayerCount);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);
        Debug.Log("Create a new Room");
        PhotonNetwork.CreateRoom(null, new RoomOptions {MaxPlayers = 4, IsVisible = true, IsOpen = true});
    }

    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        Debug.Log("Room Created");
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        Debug.Log("Error Create Room: " + message);
    }
}
