﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class scr_Player : MonoBehaviourPunCallbacks
{
    public float health;
    public float attackSpeed;
    public float moveSpeed;
    public float rotationSpeed;

    public Animator animator;

    bool attack = false;
    bool walk = false;
    int xDir = 0;
    int zDir = 0;
    void Start()
    {
        //Debug.Log(gameObject.GetPhotonView().IsMine);
        /*PhotonNetwork.AutomaticallySyncScene = true;
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.AppVersion = "1";
            PhotonNetwork.ConnectUsingSettings();
        }*/
    }

    // Update is called once per frame
    void Update()
    {
        //if (gameObject.GetPhotonView().IsMine)
        //{
            xDir = 0;
            zDir = 0;
            if (Input.GetKey(KeyCode.A)) xDir += -1;
            if (Input.GetKey(KeyCode.D)) xDir += 1;
            if (Input.GetKey(KeyCode.W)) zDir += 1;
            if (Input.GetKey(KeyCode.S)) zDir += -1;

            if (xDir != 0 || zDir != 0) walk = true;
            else walk = false;

            if (Input.GetMouseButton(0))
            {
                attack = true;
            }
            else attack = false;

            if (!attack) transform.Translate(new Vector3(moveSpeed * Time.deltaTime * xDir, 0, moveSpeed * Time.deltaTime * zDir));
            else transform.Translate(new Vector3((moveSpeed / 4) * Time.deltaTime * xDir, 0, (moveSpeed / 4) * Time.deltaTime * zDir));

            PUN_Animation();
        //}
    }

    [PunRPC]
    void CrearBala()
    {
        PhotonNetwork.Instantiate("Bala", Vector3.zero, Quaternion.identity);
    }

    void CrearSuperPro()
    {
        gameObject.GetPhotonView().RPC("CrearBala", RpcTarget.AllBufferedViaServer);
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    void Animation()
    {
        gameObject.GetPhotonView().RPC("PUN_Animation", RpcTarget.AllBufferedViaServer);
    }

    //[PunRPC]
    void PUN_Animation()
    {
        animator.SetBool("walk", walk);
        animator.SetBool("attack", attack);
    }
}
