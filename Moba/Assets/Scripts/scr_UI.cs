﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class scr_UI : MonoBehaviour
{
    public GameObject[] aui;
    public GameObject[] dui;

    public void ActivateUI()
    {
        foreach (GameObject i in aui)
        {
            i.SetActive(true);
        }
    }

    public void DeactivateUI()
    {
        foreach (GameObject i in dui)
        {
            i.SetActive(false);
        }
    }
}
