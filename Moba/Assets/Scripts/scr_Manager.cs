﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using Photon.Pun;
using Photon.Realtime;

public class scr_Manager : MonoBehaviourPunCallbacks
{
    //Public
    //public string playerName;
    public string gameVersion;
    //public string roomName;

    //UI
    public TextMeshProUGUI status;
    public TextMeshProUGUI rooms;
    public TextMeshProUGUI playerName;
    public TMP_InputField roomName;
    public GameObject roomUI;
    //public GameObject scrollView;

    //List<GameObject> roomsUI = new List<GameObject>();
    List<RoomInfo> createdRooms = new List<RoomInfo>();
    //End public

    bool joiningRoom = false;

    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.AppVersion = gameVersion;
            PhotonNetwork.ConnectUsingSettings();
        }
        playerName.text = playerName.text + PhotonNetwork.NickName;

        /*for (int i = 0; i < 5; i++)
        {
            roomsUI[i] = Instantiate(roomUI, scrollView.transform.position, Quaternion.identity) as GameObject;
            roomsUI[i].transform.parent = scrollView.transform;
        }*/
    }

    public void OnClickConnectToServer()
    {
        PhotonNetwork.NickName = Global.Instance.nickname;
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.GameVersion = "v0";
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        Debug.Log("Disconnected: " + cause.ToString() + " Server: " + PhotonNetwork.ServerAddress);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);
        createdRooms = roomList;
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        joiningRoom = false;
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);
        joiningRoom = false;
    }

    public void OnClickConnectToRoom()
    {
        if (!PhotonNetwork.IsConnected)
            return;

        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("Joined Room");
        Debug.Log("MASTER: " + PhotonNetwork.IsMasterClient + " | Player Name: " + PhotonNetwork.NickName + " | Room Name: " + PhotonNetwork.CurrentRoom.Name + " | Room Players: " + PhotonNetwork.CurrentRoom.PlayerCount);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);
        joiningRoom = false;
    }

    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        Debug.Log("Room Created");
        //PhotonNetwork.NickName = playerName.text;
        PhotonNetwork.LoadLevel("s_Lobby");
    }

    private void Update()
    {
        //GUI.Window(0, new Rect(Screen.width / 2 - 450, Screen.height / 2 - 200, 900, 400), LobbyWindow, "Lobby");
        LobbyStats();
    }

    private void LobbyStats()
    {
        status.text = "Status: " + PhotonNetwork.NetworkClientState;

        if (createdRooms.Count == 0)
        {
            rooms.text = "No Rooms created yet...";
        }
        else
        {

            /*for (int i = 0; i < createdRooms.Count; i++)
            {
                GUILayout.BeginHorizontal("box");
                GUILayout.Label(createdRooms[i].Name, GUILayout.Width(400));
                GUILayout.Label(createdRooms[i].PlayerCount + "/" + createdRooms[i].MaxPlayers);

                GUILayout.FlexibleSpace();

                if (GUILayout.Button("Join Room"))
                {
                    joiningRoom = true;

                    PhotonNetwork.JoinRoom(createdRooms[i].Name);
                }
                GUILayout.EndHorizontal();
            }*/

            roomUI.SetActive(true);
            roomUI.GetComponent<scr_RoomStats>().roomName = createdRooms[0].Name;
            roomUI.GetComponent<scr_RoomStats>().playerCount = createdRooms[0].PlayerCount + "/" + createdRooms[0].MaxPlayers;

            if (Global.Instance.joinRoom)
            {
                PhotonNetwork.JoinRoom(createdRooms[0].Name);
                Global.Instance.joinRoom = false;
            }

            rooms.text = "";
        }
    }

    public void OnClickCreateRoom()
    {
        if (roomName.text != "")
        {
            joiningRoom = true;

            RoomOptions roomOptions = new RoomOptions();
            roomOptions.IsOpen = true;
            roomOptions.IsVisible = true;
            roomOptions.MaxPlayers = 2;

            PhotonNetwork.JoinOrCreateRoom(roomName.text, roomOptions, TypedLobby.Default);
        }
    }

    public void OnClickRefresh()
    {
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.JoinLobby(TypedLobby.Default);
        }
        else
        {
            PhotonNetwork.ConnectUsingSettings();
        }
    }
}
