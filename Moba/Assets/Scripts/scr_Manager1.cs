﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using Photon.Pun;
using Photon.Realtime;

public class scr_Manager1 : MonoBehaviourPunCallbacks
{
    //Public
    public string playerName;
    public string gameVersion;
    public string roomName;

    List<RoomInfo> crateRooms = new List<RoomInfo>();
    //End public

    Vector2 roomListScroll = Vector2.zero;
    bool joiningRoom = false;

    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        if(!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.AppVersion = gameVersion;
            PhotonNetwork.ConnectUsingSettings();
        }

        playerName = Global.Instance.nickname;
    }

    /*private void Awake()
    {
        b_ConnectToGame.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        stats.text = "Ping: " + PhotonNetwork.GetPing() + "\n" + "Connected: " + PhotonNetwork.IsConnected;
    }*/

    public void OnClickConnectToServer()
    {
        PhotonNetwork.NickName = "Player001";
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.GameVersion = "v0";
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        Debug.Log("Disconnected: " + cause.ToString() + " Server: " + PhotonNetwork.ServerAddress);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);
        crateRooms = roomList;
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        joiningRoom = false;
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);
        joiningRoom = false;
    }

    public void OnClickConnectToRoom()
    {
        if (!PhotonNetwork.IsConnected)
            return;

        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("Joined Room");
        Debug.Log("MASTER: " + PhotonNetwork.IsMasterClient + " | Player Name: " + PhotonNetwork.NickName + " | Room Name: " + PhotonNetwork.CurrentRoom.Name + " | Room Players: " + PhotonNetwork.CurrentRoom.PlayerCount);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);
        joiningRoom = false;
    }

    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        Debug.Log("Room Created");
        PhotonNetwork.NickName = playerName;
        PhotonNetwork.LoadLevel("s_Lobby");
    }

    private void OnGUI()
    {
        GUI.Window(0, new Rect(Screen.width / 2 - 450, Screen.height / 2 - 200, 900, 400), LobbyWindow, "Lobby");
    }

    void LobbyWindow(int index)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Status: " + PhotonNetwork.NetworkClientState);

        if(joiningRoom || !PhotonNetwork.IsConnected || PhotonNetwork.NetworkClientState != ClientState.JoinedLobby)
        {
            GUI.enabled = false;
        }

        GUILayout.FlexibleSpace();

        roomName = GUILayout.TextField(roomName, GUILayout.Width(250));

        if(GUILayout.Button("Create Room", GUILayout.Width(125)))
        {
            if(roomName != "")
            {
                joiningRoom = true;

                RoomOptions roomOptions = new RoomOptions();
                roomOptions.IsOpen = true;
                roomOptions.IsVisible = true;
                roomOptions.MaxPlayers = 5;

                PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
            }
        }

        GUILayout.EndHorizontal();

        roomListScroll = GUILayout.BeginScrollView(roomListScroll, true, true);

        if(crateRooms.Count == 0)
        {
            GUILayout.Label("No Rooms created yet...");
        }
        else
        {
            for(int i = 0; i < crateRooms.Count; i++)
            {
                GUILayout.BeginHorizontal("box");
                GUILayout.Label(crateRooms[i].Name, GUILayout.Width(400));
                GUILayout.Label(crateRooms[i].PlayerCount + "/" + crateRooms[i].MaxPlayers);

                GUILayout.FlexibleSpace();

                if(GUILayout.Button("Join Room"))
                {
                    joiningRoom = true;

                    PhotonNetwork.NickName = playerName;

                    PhotonNetwork.JoinRoom(crateRooms[i].Name);
                }
                GUILayout.EndHorizontal();
            }
        }
        GUILayout.EndScrollView();

        GUILayout.BeginHorizontal();

        GUILayout.Label("Player Name: ", GUILayout.Width(85));

        playerName = GUILayout.TextField(playerName, GUILayout.Width(250));

        GUILayout.FlexibleSpace();

        GUI.enabled = (PhotonNetwork.NetworkClientState == ClientState.JoinedLobby || PhotonNetwork.NetworkClientState == ClientState.Disconnected) && !joiningRoom;

        if(GUILayout.Button("Refresh", GUILayout.Width(100)))
        {
            if(PhotonNetwork.IsConnected)
            {
                PhotonNetwork.JoinLobby(TypedLobby.Default);
            }
            else
            {
                PhotonNetwork.ConnectUsingSettings();
            }
        }

        GUILayout.EndHorizontal();

        if(joiningRoom)
        {
            GUI.enabled = true;
            GUI.Label(new Rect(900 / 2 - 50, 400 / 2 - 10, 100, 20), "Connecting..");
        }
    }
}
