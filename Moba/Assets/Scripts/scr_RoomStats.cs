﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class scr_RoomStats : MonoBehaviour
{
    public string roomName;
    public string playerCount;
    public TextMeshProUGUI textRoomName;
    public TextMeshProUGUI textPlayerCount;

    private void Update()
    {
        textRoomName.text = roomName;
        textPlayerCount.text = playerCount;
    }

    public void OnClickJoinRoom()
    {
        Global.Instance.joinRoom = true;
    }
}
