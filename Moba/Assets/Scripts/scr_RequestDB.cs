﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;

public class scr_RequestDB : MonoBehaviour
{
    public TMP_InputField user;
    public TMP_InputField password;

    void Start()
    {
        /*PhotonNetwork.AutomaticallySyncScene = true;
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.AppVersion = "1";
            PhotonNetwork.ConnectUsingSettings();
        }*/
    }

    void Update()
    {

    }

    public void Registro()
    {
        if ((user.text != "" || user.text != null) && (password.text != "" || password.text != null))
            StartCoroutine(registro());
    }

    public void Inicio()
    {
        if((user.text != "" || user.text != null) && (password.text != "" || password.text != null))
            StartCoroutine(inicio());
    }

    IEnumerator registro()
    {
        WWWForm form = new WWWForm();

        form.AddField("user", user.text);
        form.AddField("password", password.text);

        UnityWebRequest www = UnityWebRequest.Post("https://bestmoba.000webhostapp.com/createUser.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);

        }
        else
        {
            Debug.Log("Send Complete");
            Debug.Log(www.downloadHandler.text);
            Global.Instance.nickname = user.text;
            Global.Instance.connected = true;
            /*PhotonNetwork.NickName = Global.Instance.nickname;
            PhotonNetwork.AutomaticallySyncScene = true;
            PhotonNetwork.GameVersion = "v0";*/
            //PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.LoadLevel("s_Rooms");
        }
    }

    IEnumerator inicio()
    {
        WWWForm form = new WWWForm();

        form.AddField("user", user.text);
        form.AddField("password", password.text);

        UnityWebRequest www = UnityWebRequest.Post("https://bestmoba.000webhostapp.com/login.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);

        }
        else
        {
            Debug.Log("Send Complete");
            Debug.Log(www.downloadHandler.text);
            Global.Instance.nickname = user.text;
            Global.Instance.connected = true;
           /* PhotonNetwork.NickName = Global.Instance.nickname;
            PhotonNetwork.AutomaticallySyncScene = true;
            PhotonNetwork.GameVersion = "v0";*/
            //PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.LoadLevel("s_Rooms");
        }
    }
}
