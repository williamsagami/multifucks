﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class scr_MasterGame : MonoBehaviourPunCallbacks
{
    public TextMeshProUGUI stats;
    public Image[] playerImages;
    public GameObject lobbyPanel; 
    public TextMeshProUGUI[] playerNames;
    bool lobby = true;

    public void changeColor(int _index)
    {
        gameObject.GetPhotonView().RPC("ChangeColor", RpcTarget.AllBufferedViaServer);
    }
    [PunRPC]
    void ChangeColor(int _index)
    {
        playerImages[_index].color = Color.green;
    }

    public void turnOffLobby()
    {
        gameObject.GetPhotonView().RPC("TurnOffLobby", RpcTarget.AllBufferedViaServer);
    }
    [PunRPC]
    void TurnOffLobby()
    {
        lobbyPanel.SetActive(false);
    }

    public void changeName(string _name, int _index)
    {
        gameObject.GetPhotonView().RPC("ChageName", RpcTarget.AllBufferedViaServer, _name, _index);
    }

    [PunRPC]
    void ChangeName(string _name, int _index)
    {
        playerNames[_index].text = _name;
    }

    public void Create()
    {
        gameObject.GetPhotonView().RPC("CreatePlayer", RpcTarget.AllViaServer);
    }

    [PunRPC]
    void CreatePlayer()
    {
        PhotonNetwork.Instantiate("Footman", Vector3.zero + new Vector3(Random.Range(0, 5), 0, Random.Range(0, 5)), Quaternion.identity);
    }

    void Update()
    {
        if(lobby)
        {
            if(PhotonNetwork.IsMasterClient)
            {
                stats.text = "Host";
                if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
                {
                    changeColor(0);
                    //changeName(PhotonNetwork.PlayerList[0].NickName, 0);
                }
                else if(PhotonNetwork.CurrentRoom.PlayerCount == 2)
                {
                    changeColor(1);
                    //changeName(PhotonNetwork.PlayerList[1].NickName, 1);

                    turnOffLobby();
                    Create();
                    lobby = false;
                }
            }
            else
            {
                stats.text = "Guest";
            }
        }
    }
}
